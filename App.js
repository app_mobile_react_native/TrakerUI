//import 'react-native-gesture-handler';
import * as React from 'react';
import { Button, View, StyleSheet, SafeAreaView, Text, ScrollView, TouchableOpacity, Image, requireNativeComponent } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { Home } from './src/screen/Home'
import { Account } from './src/screen/account/Account'
import { RfidProduct } from './src/screen//rfidProduct/RfidProduct'
import { Category } from './src/screen/category/Category'
import { Tienda } from './src/screen/tienda/Tienda'
import { Producto } from './src/screen/product/Producto'


const HomeStack = createStackNavigator();
const Tab = createBottomTabNavigator();

const navOptionHandler = () => ({
  headerShown: false
})

function HomeStackScreen() {
  return (
    <HomeStack.Navigator initialRouteName="Home">
      <HomeStack.Screen name="Home" component={Home} options={navOptionHandler} />
    </HomeStack.Navigator>
  )
}

const screenOptions = ({ route }) => ({
  tabBarIcon: ({ focused, color, size }) => {
    let iconName;
    let iconColor;

    if (route.name === 'Home') {
      iconName = "home"
      iconColor = focused
        ? "#ff1a1a" //require('./src/static/img/home-black.png')
        : "#433d3d" //require('./src/static/img/home.png');
    } else if (route.name === 'Categoria') {
      iconName = "list"
      iconColor = focused ?  "#ff1a1a" : "#433d3d";
    } else if (route.name === 'Account') {
      iconName = "user"
      iconColor = focused ?  "#ff1a1a" : "#433d3d";
    } else if (route.name === 'Nuevos') {
      iconName = "wifi" 
      iconColor = focused ?  "#ff1a1a" : "#433d3d";  
    }

    return <Icon name={iconName} size={24} color={iconColor} style={{ alignItems: 'center', justifyContent: 'center'}} />;
  },
})
/*
export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator screenOptions={screenOptions} tabBarOptions={{ activeTintColor: 'red', inactiveTintColor: 'black' }} >
        <Tab.Screen name="Home" component={HomeStackScreen} />
        <Tab.Screen name="Categoria" component={Category} />
        <Tab.Screen name="Nuevos" component={RfidProduct} />
        <Tab.Screen name="Account" component={Account} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
*/

function HomeTabNavigator() {
  return (
    <Tab.Navigator screenOptions={screenOptions} tabBarOptions={{ activeTintColor: 'red', inactiveTintColor: 'black' }} >
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Categoria" component={Category} />
      <Tab.Screen name="Nuevos" component={RfidProduct} />
      <Tab.Screen name="Account" component={Account} />
    </Tab.Navigator>
  )
}

export default function App() {
  return (
    <NavigationContainer>
      <HomeStack.Navigator initialRouteName="HomeTab">
        <HomeStack.Screen name="HomeTab" component={HomeTabNavigator} options={navOptionHandler} />
        <HomeStack.Screen name="Tienda" component={Tienda} />
        <HomeStack.Screen name="Producto" component={Producto} options={navOptionHandler} />
      </HomeStack.Navigator>
    </NavigationContainer>
  );
}



/*
export default function App() {
return (
  <View style={styles.container}>
    <Text>Open up App.js to start working on your app!</Text>
  </View>
);
}
*/
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
