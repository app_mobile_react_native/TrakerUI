import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, StyleSheet, TextInput } from 'react-native';
//import Icon from 'react-native-vector-icons/FontAwesome5';

export class CardItem extends Component {
    render() {
        let { navigation } = this.props
        //console.log(this.props.rating)
        return (
            //onPress={this.resPress.bind(this, yourData)}
            <TouchableOpacity onPress={() => navigation.navigate('Producto', {name: this.props.itemName})}>
                <View style={style.cardItem}>
                    <View style={style.cardItemLeft}>
                        {
                            this.props.rating > 0 ?
                                <Image style={{ height: 90, width: 60 }} source={this.props.imageUri} resizeMode='contain' />
                                :
                                <Image style={{ height: 90, width: 60, resizeMode: 'contain', }}
                                    source={{ uri: this.props.imageUri }} resizeMode='contain' />
                        }
                    </View>
                    <View style={style.cardItemRight}>
                        <Text>{this.props.itemName}</Text>
                        <Text style={{ color: 'grey', fontSize: 11 }}>{this.props.itemCreator}</Text>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#c4402f' }}>{this.props.itemPrice}</Text>
                        <Text >
                            <Text style={{ color: 'grey', fontWeight: '300', fontSize: 11 }}>You save </Text>
                        ${this.props.savings}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>

        )
    }
}
//<View style={{ height: 150, alignItems: 'center', justifyContent: 'center', borderBottomWidth: 2 }}>
const style = StyleSheet.create({
    cardItem: {
        flexDirection: 'row',
        paddingVertical: 10,
        backgroundColor: 'white'
    },
    cardItemLeft: {
        flex: 1,
        paddingLeft: 10,
    },
    cardItemRight: {
        flex: 4,
        alignItems: 'flex-start',
        height: 90,
    }
})