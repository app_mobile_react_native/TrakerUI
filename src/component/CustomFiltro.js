import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, StyleSheet, StatusBar, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

export class CustomHFiltro extends Component {
    render() {
        let { title, navigation } = this.props
        return (
            <View style={style.filter}>
                <TouchableOpacity style={style.bottomFilter}
                    onPress={() => navigation.navigate('Categoria')}>

                    <View style={{ backgroundColor: '#ff1a1a', ...style.imagenFilter }}>
                        <Icon name={'list'} size={32} color='white' style={{ alignItems: 'center', justifyContent: 'center' }} />
                    </View>
                    <Text style={{}}>Categorías</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.bottomFilter}
                    onPress={() => navigation.navigate('Tienda')}>
                    <View style={{ backgroundColor: '#feaf04', ...style.imagenFilter }}>
                        <Icon name={'store'} size={32} color='white' style={{ alignItems: 'center', justifyContent: 'center' }} />
                    </View>
                    <Text>Tiendas</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.bottomFilter}
                    onPress={() => navigation.navigate('Ofertas')}>
                    <View style={{ backgroundColor: '#feaf04', ...style.imagenFilter }}>
                        <Icon name={'bolt'} size={32} color='white' style={{ alignItems: 'center', justifyContent: 'center' }} />
                    </View>
                    <Text>Ofertas</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const style = StyleSheet.create({
    filter: {
        flexDirection: 'row',
        backgroundColor: "transparent",
        paddingVertical: 10,
        paddingHorizontal: 5,
        justifyContent: "space-evenly",
        alignItems: "flex-start",
    },
    bottomFilter: {
        alignItems: 'center',
        justifyContent: 'center',
        //marginRight: 10,
        paddingHorizontal: 5
    },
    imagenFilter: {
        alignItems: "center",
        borderRadius: 50,
        height: 50,
        width: 50,
        justifyContent: 'center',
    },
})