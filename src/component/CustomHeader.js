import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, StyleSheet, StatusBar, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
//import { IMAGE } from "./constants/Image";

export class CustomHeader extends Component {
    render() {
        let { title, navigation } = this.props
        return (

            <View style={style.header}>
                {/*<StatusBar barStyle="light-content"
                    backgroundColor="#3a455c" //color lila
        />*/}

                <StatusBar translucent backgroundColor="transparent"  />
                <View style={style.headerLeft}>
                    <Icon name="amazon" size={32} color="#ffffff" style={{ alignItems: 'center', justifyContent: 'center' }} />
                </View>
                <View style={style.headerRight}>
                    <View style={{
                        backgroundColor: 'white', paddingHorizontal: 5,
                        borderRadius: 50, flex: 1, flexDirection: "row", alignItems: 'center', justifyContent: 'space-between', height: '100%'
                    }} >
                        <Icon name="search" style={{ fontSize: 20, paddingTop: 5, color: "rgba(0,0,0,0.5)" }} />
                        <TextInput placeholder="Ingrese texto a buscar" style={{ flex: 1, marginRight: 5, paddingHorizontal: 10, borderRadius: 50 }} />

                    </View>
                </View>
            </View>


        )
    }
}

const style = StyleSheet.create({
    header: {
        flexDirection: 'row',
        height: 80,
        backgroundColor: "#3a455c",
        paddingVertical: 10,
        paddingHorizontal: 10
    },
    headerLeft: {
        flex: 1,
        flexDirection: "row",
        justifyContent: 'flex-start',
        alignItems: "center",
        paddingTop: 30
    },
    headerRight: {
        flex: 6,
        marginLeft: 5,
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: "center",
        paddingTop: 30
    }
})