import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, StyleSheet, StatusBar, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

//para los menus
//https://github.com/jaysoo/react-native-menu#adding-feedback-to-menutrigger-and-menuoption-components
//ttps://www.npmjs.com/package/react-native-options-menu
export class CustomHeaderProduct extends Component {
    render() {
        let {  navigation } = this.props
        
        return (
            <View style={style.header}>
                <StatusBar translucent backgroundColor="transparent"  />
                <View style={style.headerLeft}>
                    <TouchableOpacity onPress={() => navigation.goBack()} >
                        <Icon name="arrow-left" size={18} color="#ffffff"  style={{ alignItems: 'center', justifyContent: 'center' }} />
                    </TouchableOpacity>
                </View>
                <View style={style.headerCenter}>
                    <Text style={{ fontSize: 18, fontWeight: "700", color: "white" }}>Detalles</Text>
                </View>
                <View style={style.headerRight}>
                    <TouchableOpacity >
                        <Icon name="shopping-cart" size={18} color="#ffffff" style={{ alignItems: 'center', justifyContent: 'center' }} />
                    </TouchableOpacity>

                    <TouchableOpacity >
                        <Icon name="share" size={18} color="#ffffff" style={{ alignItems: 'center', justifyContent: 'center' }} />
                    </TouchableOpacity>

                    <TouchableOpacity >
                        <Icon name="arrow-right" size={18} color="#ffffff" style={{ alignItems: 'center', justifyContent: 'center' }} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const style = StyleSheet.create({
    header: {
        flexDirection: 'row',
        //height: 30,
        backgroundColor: "transparent",
        paddingVertical: 10,
        paddingHorizontal: 10,
        position: "absolute",
        left: 0,
        right: 0,
        top: 30,
        zIndex: 100
    },
    headerLeft: {
        flex: 1,
        flexDirection: "row",
        justifyContent: 'flex-start',
        alignItems: "center",
    },
    headerCenter: {
        flex: 3,

        //marginLeft: 5,
        //flexDirection: "row",
        //justifyContent: 'center',
        //alignItems: "center",
    },
    headerRight: {
        flex: 2,
        marginLeft: 5,
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: "center",
    }
})