import React, { Component } from 'react'
import { AppRegistry, StyleSheet, Text, View, Dimensions, Image } from 'react-native'

import Swiper from 'react-native-swiper'
import { CustomHeaderProduct } from '../component/CustomHeaderProduct'


export const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  swiper: {
    height: 350,
    marginTop: 0,
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    flex: 1,
    resizeMode: 'contain'
  },
  text: {
    color: '#fff',
    fontSize: 12,
    fontWeight: 'bold'
  }
})

export class SwiperProductCarroucel extends Component {
  render() {
    let { navigation } = this.props
    return (
      <View style={[], styles.swiper}>
        <CustomHeaderProduct navigation={navigation} />
        <Swiper showsButtons={false} autoplay loop >
          <View style={styles.slide}>
            <Image style={styles.image} source={require('../static/carroucel/swiper_2.jpg')} />
          </View>
          <View style={styles.slide}>
            <Image style={styles.image} source={require('../static/carroucel/swiper_3.jpg')} />
          </View>
          <View style={styles.slide}>
            <Image style={styles.image} source={require('../static/carroucel/swiper_2.jpg')} />
          </View>
        </Swiper>
      </View>

    )
  }
}

//AppRegistry.registerComponent('SwiperCarroucel', () => SwiperCarroucel)