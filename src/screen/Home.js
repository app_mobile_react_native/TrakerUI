import React, { Component } from 'react'
import { Text, Button, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, StatusBar, Dimensions, ScrollView } from 'react-native';
//import Icon from 'react-native-vector-icons/FontAwesome5';
//import { IMAGE } from "./constants/Image";

import { CustomHeader } from '../component/CustomHeader'
import { CustomHFiltro } from '../component/CustomFiltro'
import { CardItem } from '../component/CardItem'

const { height } = Dimensions.get('window')
export class Home extends Component {

  state = {
    scrollViewHeight: 0,
    users: [],
    page: 1
  }

  componentDidMount() {

  }

  onContentSizeChange = (contentWidth, contentHeight) => {
    this.setState({ scrollViewHeight: contentHeight })
  }

  onMomentumScrollEnd = async (nativeEvent) => {

    let current_y_position = nativeEvent.nativeEvent.contentOffset.y
    console.log("current position " + current_y_position)
    console.log("this.state.scrollViewHeight " + this.state.scrollViewHeight + "--" + (this.state.scrollViewHeight / 6 - 50))

    if (current_y_position >= (this.state.scrollViewHeight / 6 - 50)) {
      console.log("redibujando ....")

      fetch('https://jsonplaceholder.typicode.com/photos?_limit=10&_page=' + this.state.page)
        .then(res => res.json())
        .then(users => {
          const listUsers = users.map((photo) => {
            return (
              <CardItem key={photo.id}
                //imageUri={photo.thumbnailUrl+".jpg"}  ///`
                imageUri={'https://pbs.twimg.com/profile_images/486929358120964097/gNLINY67_400x400.png'}
                //imageUri={require(`${photo.thumbnailUrl}`) }  //{uri: this.props.source}  //source={{uri: this.props.source}}
                itemName={photo.title}
                itemCreator={photo.title}
                itemPrice={'$' + photo.id}
                savings={'2.5'}
                rating={0}
                navigation={this.props.navigation} />
            )
          }
          );
          this.setState({ users: this.state.users.concat(listUsers), page: this.state.page + 1 })
        })
    }
  }


  handleScroll = (event) => {
    console.log("ypos", event.nativeEvent.contentOffset.y);
  }

  render() {
    //const scrollEnabled = this.state.screenHeight > height / 2
    const scrollEnabled = true
    let { title, navigation } = this.props
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomHeader title="Home" navigation={navigation} />
        <View style={{ flex: 1, marginTop: 10 }}>
          <ScrollView style={{ flex: 1, marginTop: 0 }}
            scrollEnabled={scrollEnabled}
            onContentSizeChange={this.onContentSizeChange}
            onMomentumScrollEnd={this.onMomentumScrollEnd}
            scrollEventThrottle={16}  //valido para ios, frecuencia de desplazamiento
            onScroll={this.handleScroll}
            automaticallyAdjustContentInsets={true}
          >
            <CustomHFiltro navigation={navigation} />
            <View style={{
              marginTop: 10, paddingHorizontal: 10, paddingRight: 5,
              backgroundColor: 'white', paddingVertical: 10,
              borderBottomWidth: 1, borderBottomColor: '#dee0e2'
            }}>
              <Text>Your Recommendations</Text>
            </View>

            <View style={{ flex: 1, marginTop: 0 }}>

              <CardItem imageUri={require('../static/img/recommended_1.jpg')}
                itemName={'You can heal your life'}
                itemCreator={'Louise Hay'}
                itemPrice={'$10'}
                savings={'2.5'}
                rating={5}
                navigation={navigation} />

              <CardItem imageUri={require('../static/img/recommended_2.jpg')}
                itemName={'Uncharted 4'}
                itemCreator={'Sony'}
                itemPrice={'$19.99'}
                savings={'17'}
                rating={5}
                navigation={navigation} />

              <CardItem imageUri={require('../static/img/recommended_3.jpg')}
                itemName={'Ea UFC 3'}
                itemCreator={'Ea Sport'}
                itemPrice={'$44'}
                savings={'6'}
                rating={3}
                navigation={navigation} />

              <CardItem imageUri={require('../static/img/recommended_1.jpg')}
                itemName={'You can heal your life'}
                itemCreator={'Louise Hay'}
                itemPrice={'$10'}
                savings={'2.5'}
                rating={5}
                navigation={navigation} />

              {this.state.users}

            </View>
            <View style={{ marginTop: 0, borderWidth: 1, borderColor: 'red' }}>
              <Text>Loading</Text>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    )
  }
}



//https://aboutreact.com/react-native-flatlist-pagination-to-load-more-data-dynamically-infinite-list/
//react-native scrollview dynamic content request pagination