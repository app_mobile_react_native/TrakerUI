import React, { Component } from 'react'
import { Text, Button, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, StatusBar, Dimensions, ScrollView } from 'react-native';

//import { Producto } from './src/screen/product/Producto'
import { SwiperProductCarroucel } from '../../component/SwiperProductCarroucel'

export class Producto extends Component {

    render() {
        let { navigation } = this.props
        return (
            <ScrollView style={{ flex: 1 }}>
                <SwiperProductCarroucel navigation={navigation} />
                
                <View>

                </View>
            </ScrollView>

        )
    }
}
